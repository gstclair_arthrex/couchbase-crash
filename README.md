## System Info:

    gstclair@gstclair-Virtual-Machine:~/Development/couchbase-crash$ node --version
    v12.18.4
    gstclair@gstclair-Virtual-Machine:~/Development/couchbase-crash$ npm --version
    6.14.6
    gstclair@gstclair-Virtual-Machine:~/Development/couchbase-crash$ lsb_release -a
    No LSB modules are available.
    Distributor ID: Ubuntu
    Description:    Ubuntu 20.04.2 LTS
    Release:        20.04
    Codename:       focal


## Debug

System Hangs while debugging if `metaScraper([])` is uncommented, even if connection string is invalid:

    gstclair@gstclair-Virtual-Machine:~/Development/couchbase-crash$ npm run dev

    > couchbase-crash@1.0.0 dev /home/gstclair/Development/couchbase-crash
    > cross-env NODE_ENV=development DEBUG=couchnode* ts-node-dev --respawn --transpile-only ./src/index.ts

    [INFO] 10:52:12 ts-node-dev ver. 1.1.8 (using ts-node ver. 9.1.1, typescript ver. 4.4.3)
    Connecting to couchbase server
    couchnode:lcb:info (instance @ ../deps/lcb/src/instance.cc:521) Version=3.2.1+13-njs, Changeset=204a528618e9a86007b85c109fd46a45c21aa053 +0ms
    couchnode:lcb:info (instance @ ../deps/lcb/src/instance.cc:522) Effective connection string: couchbase://[SERVER_NAME]?network=external&config_node_timeout=5000&config_total_timeout=5000&enable_tracing=on&enable_operation_metrics=on&client_string=couchnode%2F3.2.2%20(node%2F12.18.4%3B%20v8%2F7.8.279.23-node.39%3B%20ssl%2F1.1.1g). Bucket=(null) +1ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:389) Applying initial cntl network=external +0ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:389) Applying initial cntl config_node_timeout=5000 +0ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:389) Applying initial cntl config_total_timeout=5000 +0ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:389) Applying initial cntl enable_tracing=on +0ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:389) Applying initial cntl enable_operation_metrics=on +0ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:389) Applying initial cntl client_string=couchnode/3.2.2 (node/12.18.4; v8/7.8.279.23-node.39; ssl/1.1.1g) +0ms
    couchnode:lcb:info (instance @ ../deps/lcb/src/instance.cc:212) DNS SRV lookup failed: LCB_ERR_UNKNOWN_HOST (1049). Ignore this if not relying on DNS SRV records +22ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:155) Adding host [SERVER_NAME]:8091 to initial HTTP bootstrap list +22ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:155) Adding host [SERVER_NAME]:11210 to initial CCCP bootstrap list +0ms
    couchnode:lcb:trace (instance @ ../deps/lcb/src/instance.cc:193) Bootstrap hosts loaded (cccp:0, http:0) +0ms

## Run

Segfault if `metaScraper([])` is uncommented when executing transpiled js:

    gstclair@gstclair-Virtual-Machine:~/Development/couchbase-crash$ node dist/index.js
    Connecting to couchbase server
    couchnode:lcb:info (instance @ ../deps/lcb/src/instance.cc:521) Version=3.2.1+13-njs, Changeset=204a528618e9a86007b85c109fd46a45c21aa053 +0ms
    couchnode:lcb:info (instance @ ../deps/lcb/src/instance.cc:522) Effective connection string: couchbase://[SERVER_NAME]?network=external&config_node_timeout=5000&config_total_timeout=5000&enable_tracing=on&enable_operation_metrics=on&client_string=couchnode%2F3.2.2%20(node%2F12.18.4%3B%20v8%2F7.8.279.23-node.39%3B%20ssl%2F1.1.1g). Bucket=(null) +1ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:389) Applying initial cntl network=external +0ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:389) Applying initial cntl config_node_timeout=5000 +0ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:389) Applying initial cntl config_total_timeout=5000 +0ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:389) Applying initial cntl enable_tracing=on +0ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:389) Applying initial cntl enable_operation_metrics=on +0ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:389) Applying initial cntl client_string=couchnode/3.2.2 (node/12.18.4; v8/7.8.279.23-node.39; ssl/1.1.1g) +0ms
    couchnode:lcb:info (instance @ ../deps/lcb/src/instance.cc:212) DNS SRV lookup failed: LCB_ERR_UNKNOWN_HOST (1049). Ignore this if not relying on DNS SRV records +23ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:155) Adding host [SERVER_NAME]:8091 to initial HTTP bootstrap list +23ms
    couchnode:lcb:debug (instance @ ../deps/lcb/src/instance.cc:155) Adding host [SERVER_NAME]:11210 to initial CCCP bootstrap list +0ms
    couchnode:lcb:trace (instance @ ../deps/lcb/src/instance.cc:193) Bootstrap hosts loaded (cccp:0, http:0) +0ms
    Segmentation fault (core dumped)