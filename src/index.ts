import { connect } from 'couchbase'
import metaScraper from 'metascraper'

function status() {
    console.info('Still here')
    setTimeout(status, 1000)
}

;(async () => {
    console.info(`Connecting to couchbase server`)
    metaScraper([]) // THIS CAUSES ISSUES W/ COUCHBASE, ESPECIALLY WHEN RUN in VS Code's Javascript Debug Terminal 
    const cluster = await connect(
        'couchbase://[SERVER_NAME]?network=external&config_node_timeout=5000&config_total_timeout=5000',
        {
            username: '[USERNAME]',
            password: '[PASSWORD]'
        }
    )
    console.info(`Connected to couchbase server`)
    status()
})()
